# El Gamal Crypto-System POC

A proof-of-concept for the El Gamal cryptographic system used by the Echo Protocol.

The Citizen Client encrypts a ballot and generates a proof of knowledge of the ephemeral key as well as a disjunctive proof of encryption of plaintext.

The VoDI Engine verifies those cryptographic proofs, aggregates the encrypted votes, decrypts the result and computes the final tally.
