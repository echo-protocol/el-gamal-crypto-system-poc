const JSBN =  require('jsbn');
const BigInt = JSBN.BigInteger;
const ElGamal = require('./services/elgamal');
const Proofs = require('./services/proofs');
const Utils = require('./utils/utils');

const nbVotes = 10;
const nbChoices = 5;
const bitLength = 2048;

runVote(nbVotes, nbChoices, bitLength);

async function runVote(nbVotes, nbChoices, bitLength){
  return new Promise(async (resolve, reject) => {
    try{

      const validPlainTextsBigInt = [new BigInt("2", 10), new BigInt("3", 10), new BigInt("5", 10), new BigInt("7", 10), new BigInt("11", 10)].slice(0, nbChoices);

      const { pBigInt, gBigInt, ABigInt, skBigInt } = await instantiateVote(bitLength);

      let maxNbAggregatedVotes = computeMaxNbAggregatedVotes(validPlainTextsBigInt[validPlainTextsBigInt.length - 1], bitLength);

      console.log("MAX NUMBER AGGREGATED VOTES = " + maxNbAggregatedVotes + "\n");

      const { aggregatedVotesList, votesList } = await castVotes(pBigInt, gBigInt, ABigInt, validPlainTextsBigInt, nbVotes, 0, null, [], 0, [], maxNbAggregatedVotes);

      const decryptedAggregatedVoteBigInt = await decryptAggregatedVotesList(aggregatedVotesList, skBigInt, gBigInt, pBigInt, null, 0);

      const votesCount = countVotes(validPlainTextsBigInt, decryptedAggregatedVoteBigInt);

      checkVotesCount(validPlainTextsBigInt, votesList, votesCount);

    } catch(e){ reject(e) }
  })
}


function instantiateVote(bitLength) {
  return new Promise(async (resolve, reject) => {
    try {

      const {pBigInt, gBigInt, ABigInt, skBigInt} = await ElGamal.generateKeyPairAsync(bitLength);

      resolve({pBigInt, gBigInt, ABigInt, skBigInt})

    } catch (e) {
      reject(e)
    }
  });
}

async function castVotes(pBigInt, gBigInt, ABigInt, validPlainTextsBigInt, nbVotes, index, aggregatedVoteBigInt, aggregatedVotesList, aggregatedVotesIndex, votesList, maxNbAggregatedVotes){

  return new Promise(async (resolve, reject) => {

    try{

      if(index < nbVotes) {

        console.log("VOTE NB: " + (index + 1).toString() + "\n");

        const mBigInt = validPlainTextsBigInt[parseInt((await Utils.getRandomBigIntAsync(new BigInt("0", 10), new BigInt((validPlainTextsBigInt.length - 1).toString(), 10))).toString(10), 10)];

        console.log("Plaintext m =", mBigInt.toString(10), "\n");

        const {c1BigInt, c2BigInt, kBigInt} = await ElGamal.encryptMessage(mBigInt, pBigInt, gBigInt, ABigInt);

        const POKofK = await Proofs.generatePOKOfEphemeralKey(kBigInt, gBigInt, c1BigInt, pBigInt);

        const verifPOK = Proofs.verifyPOKOfEphemeralKey(POKofK.aBigInt, POKofK.eBigInt, POKofK.zBigInt, gBigInt, c1BigInt, pBigInt);

        if(verifPOK){

          const DZKPOP = await Proofs.generateDisjunctiveZKProofOfPlaintext(kBigInt, mBigInt, validPlainTextsBigInt, gBigInt, ABigInt, c2BigInt, pBigInt);

          const verifDZKPOP = Proofs.verifyDisjunctiveProofOfPlaintext(validPlainTextsBigInt, gBigInt, ABigInt, c2BigInt, pBigInt, DZKPOP.commitments, DZKPOP.challenges, DZKPOP.responses);

          if(verifDZKPOP){

            if(aggregatedVoteBigInt){
              aggregatedVoteBigInt = { C1BigInt: aggregatedVoteBigInt.C1BigInt.multiply(c1BigInt), C2BigInt: aggregatedVoteBigInt.C2BigInt.multiply(c2BigInt) }
            } else {
              aggregatedVoteBigInt = { C1BigInt: c1BigInt, C2BigInt: c2BigInt }
            }

            if(aggregatedVotesIndex === maxNbAggregatedVotes - 1){
              aggregatedVotesList.push(aggregatedVoteBigInt);
              aggregatedVoteBigInt = null;
              aggregatedVotesIndex = 0;
            } else {
              aggregatedVotesIndex += 1;
            }

            votesList.push(mBigInt);

            resolve(castVotes(pBigInt, gBigInt, ABigInt, validPlainTextsBigInt, nbVotes, index + 1, aggregatedVoteBigInt, aggregatedVotesList, aggregatedVotesIndex, votesList, maxNbAggregatedVotes))

          } else {

            console.log("DZKPOP FAILED AT INDEX " + index +", VOTE DISCARDED\n");

            resolve(castVotes(pBigInt, gBigInt, ABigInt, validPlainTextsBigInt, nbVotes, index + 1, aggregatedVoteBigInt, aggregatedVotesList, aggregatedVotesIndex, votesList, maxNbAggregatedVotes))

          }
        } else {

          console.log("POK of k FAILED AT INDEX " + index +", VOTE DISCARDED\n");

          resolve(castVotes(pBigInt, gBigInt, ABigInt, validPlainTextsBigInt, nbVotes, index + 1, aggregatedVoteBigInt, aggregatedVotesList, aggregatedVotesIndex, votesList, maxNbAggregatedVotes))

        }

      } else {

        if(aggregatedVoteBigInt){

          aggregatedVotesList.push(aggregatedVoteBigInt);

        }

        resolve({ aggregatedVotesList, votesList })

      }

    } catch(e){ reject(e) }
  })
}

async function decryptAggregatedVote(aggregatedVoteBigInt, skBigInt, gBigInt, pBigInt){

  return new Promise(async (resolve, reject) => {

    try{

      console.log("AGGREGATED VOTE: ", aggregatedVoteBigInt.C1BigInt.toString(10), aggregatedVoteBigInt.C2BigInt.toString(10));

      let decryptedAggregatedVoteBigInt = aggregatedVoteBigInt.C1BigInt.modPow(skBigInt, pBigInt).modInverse(pBigInt).multiply(aggregatedVoteBigInt.C2BigInt).remainder(pBigInt);

      resolve(decryptedAggregatedVoteBigInt);

    } catch(e){ reject(e) }
  })
}

async function decryptAggregatedVotesList(aggregatedVotesList, skBigInt, gBigInt, pBigInt, decryptedAggregatedVoteBigInt, index){
  return new Promise(async (resolve, reject) => {
    try{

      if(index < aggregatedVotesList.length){

        if(!decryptedAggregatedVoteBigInt){

          decryptedAggregatedVoteBigInt = await decryptAggregatedVote(aggregatedVotesList[index], skBigInt, gBigInt, pBigInt);

        } else {

          decryptedAggregatedVoteBigInt = decryptedAggregatedVoteBigInt.multiply(await decryptAggregatedVote(aggregatedVotesList[index], skBigInt, gBigInt, pBigInt));

        }

        resolve(decryptAggregatedVotesList(aggregatedVotesList, skBigInt, gBigInt, pBigInt, decryptedAggregatedVoteBigInt, index + 1))

      } else {

        console.log("\nDECRYPTED AGGREGATED VOTE: ", decryptedAggregatedVoteBigInt.toString(10), "\n");

        resolve(decryptedAggregatedVoteBigInt);

      }

    } catch(e){ reject(e) }
  })
}



function countVotes(validPlainTextsBigInt, decryptedAggregatedVoteBigInt){

  let votesCount = [];

  validPlainTextsBigInt.forEach((plainTextBigInt) => {

    let i = 0;

    while (decryptedAggregatedVoteBigInt.gcd(plainTextBigInt).equals(plainTextBigInt)) {

      decryptedAggregatedVoteBigInt = decryptedAggregatedVoteBigInt.divide(plainTextBigInt);

      i += 1

    }

    votesCount.push(i);

  });

  console.log("VOTE RESULTS:\n", votesCount);

  return votesCount;

}


function checkVotesCount(validPlainTextsBigInt, votesList, votesCount){
  let actualVotesResult = [];
  validPlainTextsBigInt.forEach((plaintext) => {
    let i = 0;
    votesList.forEach((vote) => {
      if(vote.equals(plaintext)){
        i += 1;
      }
    });
    actualVotesResult.push(i);
  });
  // console.log("\nACTUAL VOTES RESULTS: ", actualVotesResult);

  let result = true;
  for(let i = 0; i < actualVotesResult.length; i++){
    if(actualVotesResult[i] !== votesCount[i]){
      result = false;
    }
  }

  if (result){
    console.log("\nSUCCESSFULLY COUNTED VOTES\n")
  } else {
    console.log("\nVOTES COUNT FAILED !!!\n")

  }
}

function computeMaxNbAggregatedVotes(maxPlainTextBigInt, bitLength){

  let product = new BigInt("1", 10);
  let i =0;
  while(product.bitLength() < bitLength){
    product = product.multiply(maxPlainTextBigInt);
    i += 1;
  }

  return i-1;
}

